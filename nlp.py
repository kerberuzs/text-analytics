# -*- coding: utf-8 -*-

# **XMLC**

import pathlib
import nltk
import string
import os
import sklearn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools
import keras

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer

from scipy import sparse
from sklearn import tree

from sklearn.base import BaseEstimator
from keras.layers import Dense, Activation, Dropout
from keras.models import Sequential
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint

from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix

from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split

from sklearn.metrics import plot_confusion_matrix
from nltk.metrics import *

nltk.download('punkt')
nltk.download('wordnet')

"""# Input"""
""" 
!pip install -q kaggle

!mkdir /root/.kaggle

from google.colab import files
files.upload()

!cp kaggle.json /root/.kaggle/
!ls -la /root/.kaggle/

!kaggle datasets download -d hsrobo/titlebased-semantic-subject-indexing
!unzip /content/titlebased-semantic-subject-indexing.zip
 """
econbiz = pd.read_csv('econbiz.csv', index_col ='fold')
#pubmed = pd.read_csv('pubmed.csv')

"""#Preprocessing

Label Binarizer
"""

def binazier(label_list,data):
  mlb = MultiLabelBinarizer(classes=label_list)
  mlb_result = mlb.fit_transform(data)
  #mlb_result = mlb.transform(data)
  #mlb_result = np.array(mlb_result.tolist())
  return mlb_result

"""## Feature Extraction"""

def tokenizer_func(data_samples_titles,token_type):
  
  title_stems = []
  title_lems = []

  ps = PorterStemmer()
  wnl = WordNetLemmatizer()
  
  title_tokens = nltk.word_tokenize(data_samples_titles)
  for title_tk in title_tokens:
    title_stems.append(ps.stem(title_tk))
    title_lems.append(wnl.lemmatize(title_tk))
    
  if token_type == stem :
    return title_stems
  else :
    return title_lems

"""## Vectorization

###tokenization based vectorizer
"""

def tfidf_tkz_vec(data_samples_titles) :
  
  data_samples_dict = {}
  feature_names_tkz = []
  shape_tkz = []
  vector = []

  tftid_tkz_vec = TfidfVectorizer(tokenizer=tokenizer_func, stop_words='english')

  for i in range(len(data_samples_titles)):
    data_samples_title = str(data_samples_titles[i])
    data_samples_dict[i] = data_samples_title.lower().translate(str.maketrans('','',string.punctuation))
    
  tftid_tkz_result = tftid_tkz_vec.fit_transform(data_samples_dict.values())
  vector.append(tftid_tkz_vec.transform(data_samples_dict.values()))
    
  feature_names_tkz.append(tftid_tkz_vec.get_feature_names())
  shape_tkz.append(tftid_tkz_result.shape)

  return vector

"""###Count vectorizer"""

def count_vec_func(data_samples_titles) :
  
  data_samples_dict = {}
  feature_names_tkz = []
  shape_tkz = []
  vector = []

  count_vec = CountVectorizer(max_df=0.95, min_df=2, max_features=n_features, stop_words='english')
  
  for i in range(len(data_samples_titles)):
    data_samples_title = str(data_samples_titles[i])
    data_samples_dict[i] = data_samples_title.lower().translate(str.maketrans('','',string.punctuation))

  count_vec_result = count_vec.fit_trasparse_output=Falsensform(data_samples_dict.values())
  vector.append(count_vec.transform(data_samples_dict.values()).toarray())
    
  feature_names_tkz.append(count_vec.get_feature_names())
  shape_tkz.append(count_vec_result.shape)

  return vector

"""###TFTID vectorizer"""

def tfidf_raw_vec_func(data_samples_titles) :

  data_samples_dict = {}
  feature_names_tkz = []
  shape_tkz = []
  vector = []

  tfidf_raw_vec = TfidfVectorizer(max_df=0.95, min_df=2, max_features=n_features, stop_words='english')

  for i in range(len(data_samples_titles)):
    data_samples_title = str(data_samples_titles[i])
    data_samples_dict[i] = data_samples_title.lower().translate(str.maketrans('','',string.punctuation))

  tftid_raw_result = tfidf_raw_vec.fit_transform(data_samples_dict.values())
  vector.append(tfidf_raw_vec.transform(data_samples_dict.values()).toarray())
  
  feature_names_tkz.append(tfidf_raw_vec.get_feature_names())
  shape_tkz.append(tftid_raw_result.shape)

  return vector

"""###Vectorization function caller"""

def vector_func(data_samples_titles) :
  
  #res_token = vars()['title_stems{}'.format(i)] = tokenizer_func(data_samples_titles,stem)
  #res_vector = tfidf_tkz_vec(data_samples_titles)
  #res_vector = count_vec_func(data_samples_titles)
  res_vector = tfidf_raw_vec_func(data_samples_titles)
  
  res_v_rows , res_v_cols = np.nonzero(res_vector[0])
  res_v_vals = res_vector[0][res_v_rows, res_v_cols]
  res_v_coo = sparse.coo_matrix((res_v_vals, (res_v_rows, res_v_cols)))
  res_v_csr = res_v_coo.tocsr()

  res_v_shape = np.shape(res_v_vals)
  res_coo_shape = np.shape(res_v_coo)
  res_csr_shape = np.shape(res_v_csr)

  #return(res_vector,res_v_coo.toarray(),res_v_csr.toarray())
  #return(res_v_shape, res_coo_shape, res_csr_shape)
  return res_vector

"""# Classification, cross validation and evaluation

##DT
"""

def DT_class_train(x_train, y_train, x_val, y_val):
  
  ## Defining decision tree classifier using cost complexity pruning
  DT_classifier = tree.DecisionTreeClassifier(criterion = "entropy")
  
  # creating decision tree classifier to find effective values of alpha
  DT_classifier.fit(x_train, y_train)
  DT_path = DT_classifier.cost_complexity_pruning_path(x_train, y_train)
  ccp_alphas, impurities = DT_path.ccp_alphas, DT_path.impurities
  
  # Training multiple pruned decision trees with different valuess of alpha 
  DT_classifiers = []
  for ccp_alpha in ccp_alphas:
    DT_classifier = tree.DecisionTreeClassifier(random_state=0, ccp_alpha=ccp_alpha)
    DT_classifier.fit(x_train, y_train)
    DT_classifiers.append(DT_classifier)

  # finding the best value of alpha based on maximum test_score
  test_scores = [DT_classifier.score(x_val, y_val) for DT_classifier in DT_classifiers]
  arg_max = np.where(test_scores == np.max(test_scores, axis=0))
  ccp_max = np.max(ccp_alphas[arg_max])
  #print("ccp_max : ", ccp_max)

  # defining the decision tree classifier based on best value of alpha

  DT_pruned = tree.DecisionTreeClassifier(random_state=0, ccp_alpha=ccp_max)
  DT_pruned = DT_pruned.fit(x_train,y_train)

  predict = DT_classifier.predict(x_val)
  
  return predict

"""##Neural Network"""

def NN_model_create(x_train, y_train, x_val, y_val, mode) :
    
    general_activation = 'relu'
    final_activation = 'softmax'
    batch_size_n = 512
    epochs_n = 20
    results = []

    x_shape = x_train.shape[1]
    y_shape = y_train.shape[1]

    # print(y_shape,x_train.shape,y_train.shape,x_val.shape,y_val.shape)
    
    model = keras.Sequential()
    model.add(keras.layers.Dense(1000, input_dim=x_shape , activation = general_activation))
    model.add(keras.layers.Dropout(0.50))
    model.add(keras.layers.Dense( y_shape , activation = final_activation))
    model.summary()
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    callbackList = [ keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)]

    if mode == 'eval' :
        history = model.fit(x_train, y_train, validation_data=(x_val, y_val), batch_size=batch_size_n, epochs=epochs_n,
                      callbacks=callbackList, verbose=2)
        results = model.predict(x_val)
        score = model.evaluate(x_val, y_val, batch_size=batch_size_n)
        print(score)
        return score,results;
    else :
        history = model.fit(x_train, y_train, batch_size=batch_size_n, epochs=epochs_n, callbacks=callbackList, verbose=2)
        #score = model.evaluate(x_val, y_val, batch_size=batch_size_n)
        results = model.predict(x_val)
        return results

"""##Result Functions"""

def pred_rev(pred) :
  for i in range(0,pred.shape[0]) :
    for j in range(0,pred.shape[1]) :
      if pred[i,j] >= tresh :
        pred[i,j] = 1
      else :
        pred[i,j] = 0
  return pred

def binazier_rev(label_list,fit_data,data) :
  mlb = MultiLabelBinarizer(classes=label_list)
  mlb_fit = mlb.fit_transform(fit_data)
  mlb_rev = mlb.inverse_transform(data)
  return mlb_rev

"""#Evaluation

## Metrics
"""

def metrics(y_val, pred) :
    
    macro_percision = precision_score(y_val, pred, average='macro')
    micro_percision = precision_score(y_val, pred, average='micro')
    
    macro_recall = recall_score(y_val, pred, average='macro')
    micro_recall = recall_score(y_val, pred, average='micro')

    f1_macro = f1_score(y_val, pred, average='macro')
    f1_micro = f1_score(y_val, pred, average='micro')

    return macro_percision,micro_percision,macro_recall,micro_recall,f1_macro,f1_micro

"""##Confusion Matrix"""

def conf_mtx(y_val, pred) :
    
    cm = ConfusionMatrix(y_val, pred)
    print(cm)
    print(cm.pretty_format(sort_by_count=True, truncate=10))

    cnf_matrix = confusion_matrix(y_val, pred)
    np.set_printoptions(precision=2)
    
    # Plot non-normalized confusion matrix
    plt.figure()
    plot_confusion_matrix(cnf_matrix, classes=range(len(set(y_val))), normalize = True,
                      title='Confusion matrix')

"""#Main"""

if __name__ == "__main__":

  print("main start !")
  
  n_samples = 1000
  n_features = 1000
  n_top_words = 20
  n_components = 20
  tresh = 0.02
  sample_fold_labels_all = []

  econ_label = np.asarray(econbiz.loc[ : , 'labels'])
  econ_title = np.asarray(econbiz.loc[ : , 'title'])

  # Fold Constructors

  for i in range(0,11) :
    vars()['sample_fold_items{}'.format(i)] = econbiz.loc[[i],['labels']]
    vars()['sample_fold_title{}'.format(i)] = econbiz.loc[[i],['title']]
    vars()['sample_fold_id{}'.format(i)] = econbiz.loc[[i],['id']]

  for i in range(0,11) :
    vars()['sample_fold_items{}'.format(i)] = np.asarray(vars()['sample_fold_items{}'.format(i)])
    vars()['sample_fold_title{}'.format(i)] = np.asarray(vars()['sample_fold_title{}'.format(i)])
    vars()['sample_fold_id{}'.format(i)] = np.asarray(vars()['sample_fold_id{}'.format(i)])

  for i in range(0,10) :
    vars()['y_train{}'.format(i)] = []
    vars()['y_val{}'.format(i)] = []
    vars()['x_train{}'.format(i)] = []
    vars()['x_val{}'.format(i)] = []

  for i in range(0,10) :
    vars()['sample_fold_labels{}'.format(i)] = []
    for j in range(0,np.shape(vars()['sample_fold_items{}'.format(i)])[0]) :
      sample_fold_labels_all = sample_fold_labels_all + str(vars()['sample_fold_items{}'.format(i)].item(j)).split("\t")
      vars()['sample_fold_labels{}'.format(i)].append(str(vars()['sample_fold_items{}'.format(i)].item(j)).split("\t")) 

  sample_fold_labels_all = np.unique(sample_fold_labels_all)

  for i in range(0,10) :
    vars()['res_vec{}'.format(i)] = vector_func(vars()['sample_fold_title{}'.format(i)])[0]
    vars()['res_bin{}'.format(i)] = binazier(sample_fold_labels_all,vars()['sample_fold_labels{}'.format(i)])
    
  for i in range(0,10) :
    vars()['x_train{}'.format(i)] = []
    vars()['y_train{}'.format(i)] = []
    for j in range(0,10) :
      if j!=i :
        for k in range(0,np.shape(vars()['res_vec{}'.format(j)])[0]):
          (vars()['x_train{}'.format(i)]).append(vars()['res_vec{}'.format(j)][k])
        for k in range(0,np.shape(vars()['res_bin{}'.format(j)])[0]):
          (vars()['y_train{}'.format(i)]).append(vars()['res_bin{}'.format(j)][k])
      else :
        vars()['x_val{}'.format(i)] = vars()['res_vec{}'.format(i)]
        vars()['y_val{}'.format(i)] = vars()['res_bin{}'.format(i)]

# Evaluation (Cross-Validation)

print("Evaluation start !")

f = open('eval_res.txt', 'w')
f.write("begin !\n")

for i in range(0,10) :
  print("Evaluation fold ", i , " start !")
  
  pred = []
  
  y_train = np.array(vars()['y_train{}'.format(i)])
  y_val = np.array(vars()['y_val{}'.format(i)])
  x_train = np.array(vars()['x_train{}'.format(i)])
  x_val = np.array(vars()['x_val{}'.format(i)])
  
  print(np.shape(y_train), np.shape(y_val), np.shape(x_train), np.shape(x_val))
  
  #pred = DT_class_train(x_train, y_train, x_val, y_val)
  
  score,pred = NN_model_create(x_train, y_train, x_val, y_val, 'eval')
  
  pred_res = pred_rev(pred)
  pred_bin = binazier_rev(sample_fold_labels_all,sample_fold_labels1,pred_res)
  y_pred = vars()['sample_fold_labels{}'.format(i)]
  
  macro_percision,micro_percision,macro_recall,micro_recall,f1_macro,f1_micro = metrics(y_val, pred_res)
  
  f.write("macro_percision : ", macro_percision,
      "micro_percision : ", micro_percision,
      "macro_recall :", macro_recall,
      "micro_recall :", micro_recall,
      "f1_macro :", f1_macro, 
      "f1_micro :", f1_micro)
  
  print("macro_percision : ", macro_percision,
        "micro_percision : ", micro_percision,
        "macro_recall :", macro_recall,
        "micro_recall :", micro_recall,
        "f1_macro :", f1_macro,
        "f1_micro :", f1_micro)
        
    #conf_mtx(y_val, pred)
f.close()

# Prediction

i = 10
sample_fold_labels_all = []
vars()['sample_fold_labels{}'.format(i)] = []
for j in range(0,np.shape(vars()['sample_fold_items{}'.format(i)])[0]) :
  sample_fold_labels_all = sample_fold_labels_all + str(vars()['sample_fold_items{}'.format(i)].item(j)).split("\t")
  vars()['sample_fold_labels{}'.format(i)].append(str(vars()['sample_fold_items{}'.format(i)].item(j)).split("\t"))    

X = vector_func(sample_fold_title10)[0]
Y = binazier(sample_fold_labels10)

print(np.shape(X),np.shape(Y))

x_train, x_val, y_train, y_val = train_test_split(X[0], Y, test_size=0.1, shuffle=False)
#pred = DT_class_train(x_train, y_train, x_val, y_val)
pred = NN_model_create(x_train, y_train, x_val, y_val, 'pred')

pred_res = pred_rev(pred)
pred_bin = binazier_rev(sample_fold_labels_all,sample_fold_labels1,pred_res)
f = open('pred_res.txt', 'w')
f.write("begin !\n")

macro_percision,micro_percision,macro_recall,micro_recall,f1_macro,f1_micro = metrics(sample_fold_labels10, pred_res)

f.write("macro_percision : ", macro_percision,
      "micro_percision : ", micro_percision,
      "macro_recall :", macro_recall,
      "micro_recall :", micro_recall,
      "f1_macro :", f1_macro, 
      "f1_micro :", f1_micro)

print("macro_percision : ", macro_percision,
      "micro_percision : ", micro_percision,
      "macro_recall :", macro_recall,
      "micro_recall :", micro_recall,
      "f1_macro :", f1_macro, 
      "f1_micro :", f1_micro)
#conf_mtx(y_val, pred)
f.close()

"""#End"""